import logging
from psycopg2 import connect


class IDBHelper:
    sqls = {
        'instru_exps_paths': '''
            select
                substring(localdriveletter, 0, 3) || egnyte_folder_path ,
                to_timestamp(exp.datecreated / 1000)
            from
                stg_infodw.sapio_rhe_nb_instru nb
            join stg_infodw.sapio_instrument i on
                nb.instrument_used = i.instrumentname
            join stg_infodw.sapio_rhe_exp exp on
                nb.experiment_name = exp.datarecordname
            where
                nb.instrument_used = %s
                and to_timestamp(exp.datecreated / 1000) > now() - interval %s
        ''',
    }

    def __init__(
            self, host=None, port=5432, database=None, user=None,
            password=None):
        self.connection = connect(
            host=host, port=port, database=database, user=user,
            password=password)

    def __del__(self):
        self.connection.close()

    def get_experiments(self, instru_name, depth):
        with self.connection.cursor() as curs:
            curs.execute(
                self.sqls['instru_exps_paths'],
                (instru_name, f'{depth} weeks'))
            result = curs.fetchall()
        logging.debug(
            f'Fetched {len(result)} experiments from the database')
        return [item[0] for item in result]
