from datetime import datetime
from os import remove, path
from unittest import TestCase
from unittest.mock import patch

from .darwinsync_request_helper import DSAPIHelper


class MockResponse:
    def __init__(
            self, status=200, json=None, reason=None, ok=True, content=None):
        self.status_code = status
        self.body = json or {}
        self.reason = reason
        self.ok = ok
        self.content = content

    def json(self):
        return self.body


class DSRequestTestCase(TestCase):
    def setUp(self):
        self.ds_api_helper = DSAPIHelper()
        self.good_auth_response = MockResponse(
            json={'access_token': 'test token'})

    @patch(
        'libs.darwinsync_request_helper.darwinsync_request_helper'
        '.requests.post')
    @patch(
        'libs.darwinsync_request_helper.darwinsync_request_helper'
        '.requests.request')
    def test_index_requested(self, mk_get, mk_post):
        test_data = 'Some test data'
        mock_response = MockResponse(json={
            'data': test_data,
            'meta': {
                'pageCount': 1,
            },
        })
        mk_get.return_value = mock_response
        mk_post.return_value = self.good_auth_response
        response = self.ds_api_helper.get_files_index(test_kwarg='test value')
        self.assertEqual(response, test_data)
        self.assertEqual(mk_get.call_count, 1)
        self.assertIn('test_kwarg', mk_get.call_args[1]['params'])
        self.assertEqual(
            mk_get.call_args[1]['params']['test_kwarg'], 'test value')
        self.assertIn('page_size', mk_get.call_args[1]['params'])
        self.assertEqual(
            mk_get.call_args[1]['params']['page_size'], 1000)

        response = self.ds_api_helper.get_files_index(
            test_kwarg='test value', page_size=13)
        self.assertEqual(response, test_data)
        self.assertEqual(mk_get.call_count, 2)
        self.assertEqual(mk_get.call_args[1]['params']['page_size'], 13)

        mock_response.body['meta']['pageCount'] = 2
        with self.assertLogs(level='DEBUG') as logs:
            response = self.ds_api_helper.get_files_index(
                test_kwarg='test value')
        self.assertEqual(len(logs.records), 2)
        self.assertIn('2 pages to request', logs.records[0].getMessage())
        self.assertIn('requesting page 2', logs.records[1].getMessage())
        self.assertEqual(response, test_data * 2)
        self.assertEqual(mk_get.call_count, 4)

    @patch(
        'libs.darwinsync_request_helper.darwinsync_request_helper'
        '.requests.request')
    @patch(
        'libs.darwinsync_request_helper.darwinsync_request_helper'
        '.requests.post')
    def test_auth(self, mk_post, mk_request):
        mk_request.return_value = MockResponse(status=401)
        mk_post.return_value = MockResponse(
            json={'access_token': 'test_token'})
        with self.assertRaises(Exception) as e:
            self.ds_api_helper.get_files_index(test_kwarg='test value')
        self.assertEqual(mk_post.call_count, 2)
        self.assertEqual(mk_request.call_count, 2)
        self.assertIn('Request failed', e.exception.args)

    @patch(
        'libs.darwinsync_request_helper.darwinsync_request_helper'
        '.requests.post')
    @patch(
        'libs.darwinsync_request_helper.darwinsync_request_helper'
        '.requests.get')
    @patch(
        'libs.darwinsync_request_helper.darwinsync_request_helper'
        '.requests.request')
    def test_file_requested(self, mk_request, mk_get, mk_post):
        test_file_path = '.\\some_test_file.txt'
        test_data = [{
            'id': 'test_id',
            'attributes': {
                'client_updated_at': datetime.now().isoformat(),
                'client_path': test_file_path,
                's3_link': 'test_s3_link',
            }}]
        mock_response = MockResponse(json={
            'data': test_data,
            'meta': {
                'pageCount': 1,
            },
        })
        mk_request.return_value = mock_response
        mk_get.return_value = MockResponse(content=b'Test content')
        mk_post.return_value = MockResponse(
            json={'access_token': 'test_token'})
        if path.exists(test_file_path):
            remove(test_file_path)
        self.ds_api_helper.download_experiment('Fake experiment')
        self.assertEqual(mk_request.call_count, 2)
        self.assertEqual(mk_get.call_count, 1)
        self.assertEqual(mk_post.call_count, 1)
        self.assertTrue(path.exists(test_file_path))
        with self.assertLogs(level='DEBUG') as logs:
            self.ds_api_helper.download_experiment('Fake experiment')
        self.assertEqual(len(logs.records), 5)
        self.assertIn('Alreay downloaded', logs.records[3].getMessage())
        remove(test_file_path)
