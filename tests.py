from datetime import datetime, timezone, timedelta
from os import path, mkdir, stat, utime, listdir
from shutil import rmtree
import unittest
from unittest.mock import patch

from cleanup import main


class MockArgs:
    def __init__(
            self, instru_name='SomeFakeInstrument', create_folders=False,
            check_bb=False, delete_obsolete=False, log_only=True,
            verbose=False):
        self.instru_name = instru_name
        self.create_folders = create_folders
        self.check_bb = check_bb
        self.delete_obsolete = delete_obsolete
        self.log_only = log_only
        self.verbose = verbose


class MockIDBConnection:
    def __init__(self, exp_list):
        self.exp_list = exp_list

    def get_experiments(self, instru_name, weeks_to_keep_alive):
        return self.exp_list


class MockDSAPIHelper:
    def __init__(self, index=None, files=None):
        self.index = index
        self.files = files

    def get_files_index(self, **kwargs):
        return self.index

    def get_files_info(self):
        return self.files


class CleanupTest(unittest.TestCase):
    def setUp(self):
        self.mk_config = {
            'GENERAL': {
                'weeks_to_keep_alive': 3,
                'weeks_to_keep_in_trash': 10,
                'trash_path': './trash',
            },
        }
        self.mk_ds_helper = MockDSAPIHelper([])

    @patch('cleanup.makedirs')
    def test_args(self, mk_makedirs):
        mk_idb_conn = MockIDBConnection(['SomeFakeExperiment'])
        with self.assertLogs(level='DEBUG') as logs:
            main(
                MockArgs(verbose=True), self.mk_config, mk_idb_conn,
                self.mk_ds_helper
            )
        self.assertEqual(len(logs.records), 2)
        with self.assertLogs(level='INFO') as logs:
            main(
                MockArgs(create_folders=True), self.mk_config, mk_idb_conn,
                self.mk_ds_helper
            )
        self.assertEqual(len(logs.records), 1)
        self.assertEqual(mk_makedirs.call_count, 1)
        with self.assertLogs(level='WARNING') as logs:
            main(
                MockArgs(check_bb=True), self.mk_config, mk_idb_conn,
                self.mk_ds_helper
            )
        self.assertEqual(len(logs.records), 1)
        mk_config = self.mk_config.copy()
        mk_config['GENERAL']['trash_path'] = 'nowhere'
        if path.exists(mk_config['GENERAL']['trash_path']):
            rmtree(mk_config['GENERAL']['trash_path'])
        with self.assertLogs(level='ERROR') as logs:
            main(
                MockArgs(check_bb=True, delete_obsolete=True), mk_config,
                mk_idb_conn, self.mk_ds_helper
            )
        self.assertEqual(len(logs.records), 1)
        self.assertIn('does not exist', logs.records[0].getMessage())

    def create_test_exp(self, file_path):
        dir, file = path.split(file_path)
        rmtree(dir, ignore_errors=True)
        mkdir(dir)
        open(file_path, 'w').close()

    def test_bb_called(self):
        test_exp_dir = path.join('.', 'testexp')
        mk_idb_conn = MockIDBConnection([test_exp_dir])
        test_exp_file = path.join(test_exp_dir, 'some_exp_file')
        self.create_test_exp(test_exp_file)
        self.assertTrue(path.exists(test_exp_file))
        mk_ds_helper = MockDSAPIHelper([
            {'attributes': {
                'client_path': test_exp_file,
                's3_size': stat(test_exp_file).st_size,
                'client_updated_at': datetime.fromtimestamp(
                    stat(test_exp_file).st_mtime).astimezone(
                    timezone.utc).strftime('%Y-%m-%dT%H:%M:%S.%fZ'),
            }}
        ])
        with self.assertLogs(level='DEBUG') as logs:
            main(
                MockArgs(check_bb=True, verbose=True), self.mk_config,
                mk_idb_conn, mk_ds_helper)
        self.assertEqual(len(logs.records), 3)
        self.assertIn('Script started', logs.records[0].getMessage())
        self.assertIn('- OK', logs.records[1].getMessage())
        self.assertIn('uploaded completely', logs.records[2].getMessage())

        with open(test_exp_file, 'a') as f:
            f.write('some extra text')
        with self.assertLogs(level='DEBUG') as logs:
            main(
                MockArgs(check_bb=True, verbose=True),
                self.mk_config, mk_idb_conn, mk_ds_helper)
        self.assertEqual(len(logs.records), 3)
        self.assertIn('Script started', logs.records[0].getMessage())
        self.assertIn('not in sync', logs.records[1].getMessage())
        self.assertIn(
            'has not been completely uploaded', logs.records[2].getMessage())

        open(test_exp_file, 'w').close()
        with self.assertLogs(level='DEBUG') as logs:
            main(
                MockArgs(check_bb=True, verbose=True),
                self.mk_config, mk_idb_conn, mk_ds_helper)
        self.assertEqual(len(logs.records), 3)
        self.assertIn('Script started', logs.records[0].getMessage())
        self.assertIn('not in sync', logs.records[1].getMessage())
        self.assertIn(
            'has not been completely uploaded', logs.records[2].getMessage())

    def test_folders_created(self):
        test_exp_path = './testexp/someexp/'
        mk_idb_conn = MockIDBConnection([test_exp_path])
        with self.assertLogs(level='DEBUG') as logs:
            main(
                MockArgs(create_folders=True), self.mk_config, mk_idb_conn,
                self.mk_ds_helper)
        self.assertEqual(len(logs.records), 3)
        self.assertIn('Script started', logs.records[0].getMessage())
        self.assertIn(' folder(s) created', logs.records[2].getMessage())
        self.assertTrue(path.exists(test_exp_path))

    @patch('cleanup.check_biobright', return_value=['testexp'])
    def test_old_folders_deleted(self, mk_check_bb):
        old_exp_path = path.join('.', 'testexp')
        old_exp_file = path.join(old_exp_path, 'oldexp')
        self.create_test_exp(old_exp_file)
        mk_idb_conn = MockIDBConnection([old_exp_path])
        mk_ds_helper = MockDSAPIHelper(index=[{
            'attributes': {
                'client_path': 'oldexp',
                's3_size': 13,
                'client_updated_at': datetime.now().strftime(
                    '%Y-%m-%dT%H:%M:%S.%fZ')
            },
        }])
        very_old_moment = (datetime.now() - timedelta(weeks=5)).timestamp()
        utime(old_exp_file, (very_old_moment, very_old_moment))
        test_trash_path = './trash'
        if path.exists(test_trash_path):
            rmtree(test_trash_path)
        main(
            MockArgs(
                check_bb=True, delete_obsolete=True, log_only=False,
                verbose=True),
            self.mk_config, mk_idb_conn, mk_ds_helper)
        self.assertIn('testexp', listdir(test_trash_path))
        rmtree(test_trash_path)
        self.assertTrue(mk_check_bb.called)
