"""This module provides useful functions to deal with BioBright DarwinSync API

URL, credentials and other input data should be set up in
darwinsync_helper.ini file.
Logging is expected to be set up in the calling module.
"""

from datetime import timezone
from dateutil import parser
import logging
from os import path, makedirs, utime
import requests


CLIENT_ID = 'ueJn7YGLF9wXgalkLl6oqu2v7LhEIuMj'


class DSAPIHelper:
    """Class for requesting info from BioBright DarwinSync"""
    def __init__(
            self, root_url=None, page_size=1000, debug=False,
            username=None, password=None):
        self.root_url = root_url or 'https://darwin.biobright.rheosrx.com/api'
        self.page_size = page_size
        self.debug = debug
        self.index_url = f'{self.root_url}/files/index'
        self.files_url = f'{self.root_url}/files'
        self.auth_url = f'{self.root_url}/oauth2/access-token'
        self.username = username
        self.password = password
        self.auth_headers = None

    def get_token(self):
        """Obtain API token to be used in requests"""
        credentials = {
            'username': self.username,
            'password': self.password,
            'nodeBioBright_client_id': CLIENT_ID,
            'scopes': ''
        }
        res = requests.post(
            self.auth_url, json=credentials,
            headers={'content-type': 'application/json'})
        if res.ok and 'access_token' in res.json():
            result = res.json()['access_token']
            logging.debug(f'new token obtained from BioBright')
            return result
        else:
            raise Exception(
                'Authorization failed: ', res.status_code, res.reason)

    def _refresh_auth_headers(self):
        self.auth_headers = {'Authorization': f'Bearer {self.get_token()}'}

    def _request_ds(self, method, url, params, allow_redirects=False):
        if not self.auth_headers:
            self._refresh_auth_headers()
        response = requests.request(
            method, url, params=params, headers=self.auth_headers,
            allow_redirects=allow_redirects)
        if response.status_code == 401:
            self._refresh_auth_headers()
            response = requests.request(
                method, url, params=params, headers=self.auth_headers,
                allow_redirects=allow_redirects)
        if response.status_code != 200:
            raise Exception(
                "Request failed", response.status_code, response.reason)
        return response.json()

    def _get_data(self, url, **kwargs):
        params = {key: kwargs[key] for key in kwargs if kwargs[key]}
        params['page_size'] = params.get('page_size', self.page_size)
        response = self._request_ds('GET', url, params)
        result = response['data']
        if 'page' in kwargs:
            return result
        logging.debug(f"{response['meta']['pageCount']} pages to request")
        for page in range(2, response['meta']['pageCount'] + 1):
            # TODO: make it async
            logging.debug(f'requesting page {page}')
            params['page'] = page
            result += self._request_ds('GET', url, params)['data']
        return result

    def get_files_index(self, **kwargs):
        """Query files metadata using /index/ endpoint.

        See https://darwin.biobright.rheosrx.com/
            docs/#/customer-facing/getMetadata
        """
        return self._get_data(self.index_url, **kwargs)

    def get_files_info(self, **kwargs):
        """Query direct files' links

        See https://darwin.biobright.rheosrx.com/
            docs/#/customer-facing/AccessFiles
        """
        return self._get_data(self.files_url, **kwargs)

    def download_experiment(self, experiment_name):
        """Download all files of the given experiment to the local machine.

        The files are downloaded to the same paths they were uploaded from.
        Make sure you have same OS (usually Windows), and the appropriate paths
        are available for writing.
        """
        files_index = self.get_files_index(client_path=experiment_name)
        logging.debug(
            f"Experiment '{experiment_name}': '"
            f"{len(files_index)} files found on BioBright.")
        counter = 1
        skipped_counter = 0
        for item in files_index[:1 if self.debug else None]:
            file_updated_at = parser.parse(
                item['attributes']['client_updated_at'])
            counter += 1
            file_path = item['attributes']['client_path']
            file_id = item['id']
            new_dir = '\\'.join(file_path.split('\\')[:-1])
            makedirs(new_dir, exist_ok=True)
            files_info = self.get_files_info(id=file_id)
            file_s3_url = files_info[0]['attributes']['s3_link']
            file_updated_at2 = parser.parse(files_info[0][
                'attributes']['client_updated_at'])
            if path.exists(file_path):
                skipped_counter += 1
                log_message = (
                    f'Alreay downloaded: "{file_path}", '
                    f'{file_updated_at} vs. {file_updated_at2}. ')
                if file_updated_at >= file_updated_at2:
                    logging.debug(log_message + 'Earlier version skipped')
                    continue
                logging.debug(log_message + 'Downloading more recent version ')
            response = requests.get(file_s3_url, allow_redirects=True)
            with open(file_path, 'wb') as f:
                f.write(response.content)
            file_updated_at = file_updated_at.replace(
                tzinfo=timezone.utc).astimezone(tz=None)
            utime(file_path, (
                file_updated_at.timestamp(), file_updated_at.timestamp()))
            logging.debug(f'file "{file_path}" downloaded')
        logging.debug(f'Experiment "{experiment_name}": downloading completed')


if __name__ == '__main__':
    logging.basicConfig(level=logging.DEBUG)
    DSAPIHelper(
        debug=True,
        username='jreed@rheosrx.com',
        password='not real one').download_experiment('Bio-445')
