"""This script performs general clean-up of the experiments on the local drives

See details in the main() function below.
The input is
    command-line options specified at the script run
        (see parsde)_args function below)
    cleanup.ini file from the same folder
The output goes to console or to log-file specified in cleanup.ini
"""

from configparser import ConfigParser
from argparse import ArgumentParser, ArgumentTypeError
from datetime import date, timedelta, datetime, timezone
from dateutil import parser
import logging
from os import walk, path, stat, makedirs, listdir
from shutil import move, rmtree

from libs.darwinsync_request_helper import DSAPIHelper
from libs.idb_helper import IDBHelper


def str2bool(v):
    """Return boolean value for string representing yes or no decision."""
    if isinstance(v, bool):
        return v
    if v.lower() in ('yes', 'true', 't', 'y', '1'):
        return True
    elif v.lower() in ('no', 'false', 'f', 'n', '0'):
        return False
    else:
        raise ArgumentTypeError('Boolean value expected.')


def parse_args():
    """Parse command-line options specified for the script."""
    arg_parser = ArgumentParser(description="Sapio data clean-up")
    arg_parser.add_argument('instru_name', type=str, help="Instrument name")
    arg_parser.add_argument(
        '-create_folders', type=str2bool, default='y',
        help='create folder structure, if absent'
    )
    arg_parser.add_argument(
        '-check_bb', type=str2bool, default='y',
        help='check BioBright sync status'
    )
    arg_parser.add_argument(
        '-delete_obsolete', type=str2bool, default='n',
        help=(
            'move to the trash bin or delete experiments '
            'older than specified in cleanup.ini')
    )
    arg_parser.add_argument(
        '-log_only', type=str2bool, default='y',
        help=(
            "don't move any files or folders, only log")
    )
    arg_parser.add_argument(
        '-verbose', type=str2bool, default='n',
        help='log everything (will be much!)'
    )
    return arg_parser.parse_args()


def check_biobright(experiments_paths, ds_api_helper):
    """Check if the files of the experiment specified are uploaded to S3.

    Results for each file are logged at DEBUG level.
    Overall result is logged at INFO level.
    """
    good_experiments = []
    for experiment_path in experiments_paths:
        is_experiment_ok = True
        if not path.exists(
                experiment_path) or len(listdir(experiment_path)) == 0:
            logging.warning(
                f'Experiment "{experiment_path} has no local files, skipping')
            continue
        files_info = ds_api_helper.get_files_index(
            client_path=experiment_path.replace('/', '\\\\'))
        files_attributes_to_check = [
            (item['attributes']['client_path'],
             int(item['attributes']['s3_size']),
             parser.parse(item['attributes']['client_updated_at']))
            for item in files_info
        ]
        for root, dirs, files in walk(experiment_path):
            for name in files:
                ds_path = path.join(root, name)
                local_file_info = stat(path.join(root, name))
                local_file_size = local_file_info.st_size
                local_file_updated_at = datetime.fromtimestamp(
                    local_file_info.st_mtime).astimezone(timezone.utc)
                if (
                    ds_path, local_file_size, local_file_updated_at
                ) in files_attributes_to_check:
                    logging.debug(f'File "{ds_path}" - OK')
                else:
                    is_experiment_ok = False
                    if ds_path in [
                            item[0] for item in files_attributes_to_check]:
                        remote_file_info = [
                            (item[1], item[2])
                            for item in files_attributes_to_check
                            if item[0] == ds_path
                        ]
                        logging.warning(
                            f'File "{ds_path}" is not in sync: '
                            'local size / update time: '
                            f'{local_file_size} / {local_file_updated_at}, '
                            'remote size(s) / update time(s): '
                            f'{remote_file_info}'
                        )
                    else:
                        logging.warning(
                            f'File "{ds_path}" is absent in BioBright')
        if is_experiment_ok:
            logging.info(
                f'Experiment "{experiment_path}" uploaded completely')
            good_experiments.append(experiment_path)
        else:
            logging.warning(
                f'Experiment "{experiment_path}" '
                'has not been completely uploaded')
    return good_experiments


def is_folder_expired(folder_path, expiration_day):
    """Check if the latest file in the given folder is older
    than the given day.

    Deep-check all the subfolders, if any.
    """
    latest_file = None
    for root, dirs, files in walk(folder_path):
        latest_file = max(
            [path.join(root, file) for file in files], key=path.getmtime)
    return bool(latest_file) and datetime.fromtimestamp(
        stat(latest_file).st_mtime).date() < expiration_day


def delete_old_folders(
        experiments_paths, trash_path, retention_period,
        retention_in_trash_period, log_only=True):
    """Move the folder to given 'trash folder' after the retention period.
    Delete the given subfolder if it is fully expired.

    Position arguments:
    experiments_paths -- list of the paths to the folder to be checked
    trash_path -- path to move the folders to after the retention period
    retention_period - retention period as a timedelta
    retention_in_trash_period - timedelta to keep in trash before deletion

    """
    makedirs(trash_path, exist_ok=True)
    for experiment_path in experiments_paths:
        if is_folder_expired(experiment_path, date.today() - retention_period):
            if not log_only:
                move(experiment_path, trash_path)
            logging.info(
                f'Experiment {experiment_path} moved to {trash_path}')
    if not path.exists(trash_path):
        logging.error(f'Trash path specified ({trash_path}) does not exist')
        return
    for experiment_path in listdir(trash_path):
        if is_folder_expired(
                experiment_path, date.today() - retention_in_trash_period):
            if not log_only:
                rmtree(experiment_path)
            logging.info(f'Experiment {experiment_path} deleted permanently')


def main(args, config, idb_connection, ds_api_helper):
    """Perform the general clean-up of the experiments kept locally.

    Request the database for the experiments that should be kept.
    Check if the folder structure is created, create if necessary.
    Check if the files have been uploaded to BioBright/S3
    Move expired experiments to the 'trash folder'
    Delete old experiments permanenty

    Arguments:
    args -- all command-line options specified at running the script
        (see 'parse_args' function)
    config -- dict with config parameters
    idb_connection -- connection to the database to request the experiments
    """
    logging.debug(f'Script started with arguments: {args}')
    if not (args.create_folders or args.check_bb or args.delete_obsolete):
        logging.debug('No actions requested, exiting')
        return

    experiments = idb_connection.get_experiments(
        args.instru_name, config['GENERAL']['weeks_to_keep_alive'])

    if args.create_folders:
        folders_count = 0
        for experiment_path in experiments:
            if not path.exists(experiment_path):
                makedirs(experiment_path)
                folders_count += 1
                logging.debug(f'Folder "{experiment_path}" created')
        logging.info(f'{folders_count} folder(s) created')

    if args.check_bb:
        uploaded_experiments = check_biobright(experiments, ds_api_helper)
        if args.delete_obsolete:
            delete_old_folders(
                uploaded_experiments, config['GENERAL']['trash_path'],
                timedelta(weeks=config['GENERAL']['weeks_to_keep_alive']),
                timedelta(weeks=config['GENERAL']['weeks_to_keep_in_trash']),
                log_only=args.log_only
            )


if __name__ == '__main__':
    args = parse_args()
    config = ConfigParser()
    config.read('cleanup.ini')
    logging.basicConfig(
        filename=config['LOGGING']['file'],
        filemode=config['LOGGING']['mode'],
        level=logging.DEBUG if args.verbose else logging.INFO,
        format='%(asctime)s %(levelname)s: %(message)s')
    trash_path = config['GENERAL']['trash_path']
    weeks_to_keep_in_trash = config['GENERAL']['weeks_to_keep_in_trash']
    idb_connection = IDBHelper(**config['DATABASE'])
    ds_api_helper = DSAPIHelper(**config['DARWINSYNC'])
    main(args, config, idb_connection, ds_api_helper)
