# Instrument clean-up Script

The overall purpose is to make sure the experiment resuls are uploaded to the
cloud.

More specifically:
## Actions
### 1. Requesting the experiments list
The list of the experiments (to be) performed on current instrument within
specified time period (usually a couple of weeks) is requested from 
Informatics Database (IDB).
### 2. Creating the directory structure
For each experiment from the list appropriate folders structure is created
in accordance to the path specified in the IDB. Skipped if already exists.
### 3. Checking if each file has been successfully uploaded in the cloud
Each file present on local disk inside of the experiment's folder is checked
by [BioBright DarwinSync API](https://darwin.biobright.rheosrx.com/docs/).
If DarwiunSync finds the file with **same path, size, and modification date**,
the conclusion is made that the file has been successfully uploaded.
### 4.Looking for expired experiments
Search for the least recent file is performed for each experiment. If the
file is older than specified in the config (see below), the folder is moved
to special "clean-up" folder (also specified by the config, usually a week
or so).

if the file is old enough (i.e. the folder is old enough generally), the
folder is **deleted permanently**. See config options below.
## Input
### Command-line options
#### Instrument name
The only positional parameter is the instrument name.
#### Options
- `create_folders` - boolean option: create directory structure as
 described above or skip this step. `true` by default.
> "boolean" means "yes", "y", "true", "t' or "1" for `true`
>or "no", "n", "false", "f" or "0" for `false` 
- `check_bb` - boolean: check upload status or skip this step. `true' by
default.
- `delete_obsolete` - boolean: move to clean-up folder and delete obsolete
data or skip this step. `false` by default. **Makes sense and works only
when `check_bb=true`, because only those experiments can be moved/deleted
that have been successfully uploaded to the cloud.**
- `log_only` - don't move or delete anything, only log. Makes sense and 
works only when `delete_obsolete=true` (and therefore `check_bb=true`)
- `verbose` - boolean: log every file checking result or only total result
for each experiment. `false` by default.

The short explanation of the command-line options is available by using
`-help` option.
### ini-file
#### `GENERAL` section
 - `debug` - put any value if you don't wish to actually move and delete
 files, but only to log the proper actions. Leave blank otherwise.
- `weeks_to_keep_alive` - number of **weeks** to retain files before moving
them to "clean-up" folder
- `weeks_to_keep_in_trash` - number of **weeks** to keep files in the 
"clean-up" folder before deletion
- `trash_path` - local path to the "clean-up" folder
#### `LOGGING` section
- `file` - filename to log to. Leave blank to log to the console.
- `mode` - "w" to start the log anew at each run, "a" to append to the
existing log.
#### `DATABASE` section
Standard data needed to connect to the IDB:
- host
- port
- database name
- username
- password 
#### `DARWINSYNC` section
- `root_url` - https://darwin.biobright.rheosrx.com/api
- `page_size` - chunk size used when requesting big amount of data
- `username`, `password` - BioBright credentials
- `debug` - put any value to request only the first file of the experiment
## Output
Logs to the file or console as described above.

Much logging at DEBUG level, disabled by default.

Overall information about experiments' statuses at INFO level.

Unexpected situations (e.g., total absence of the experiment's files on the
local drive) logged at WARNING level.
## Deployment
1. Install Python 3.7
1. Install git
1. Create target folder
1. Clone the git repository to target folder:  
`git clone http://gitlab00.rheosrx.lan/rheos-informatics/instrument-clean-up.git`
http://gitlab00.rheosrx.lan/rheos-informatics/instrument-parsers.git
1. Inside of the target folder create Python virtual environment  
`python -m venv .\venv`
1. Activate the virtual environment  
`.\venv\Scripts\activate.bat`
1. Install required packages  
`pip install -r requirements.pip`
1. Edit `cleanup.ini`. Provide valid credentials for the database, BioBright,
email. Check that trash path is reacheable. Optionally, correct other values.
1. Run script  
`python cleanup.py FIATOF01 -create_folders=n check_bb=n verbose=y`
1. Check the log file specified in `cleanup.ini` or the console for
appropriate log records.
1. If something goes wrong (error messages, odd log records, etc.), send
everything to vzherdev@rheosrx.com